package com.gg;

import com.gg.mapper.CategoryMapper;
import com.gg.service.ICategoryService;
import com.ruoyi.common.core.domain.Ztree;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("book/category")
public class CategoryController {
    String prefix = "book/category";
    @Autowired
    CategoryMapper categoryMapper;
    @Autowired
    ICategoryService iCategoryService;
    @RequestMapping("/treeData")
    @ResponseBody
    public List<Ztree> treeData() {
        return iCategoryService.getCategoryTree();

    }
    @RequestMapping("/selectCategoryTree/{categoryId}")
    public String selectCategoryTree(@PathVariable int categoryId, Model model) {
        model.addAttribute("categoryId",categoryId);
        return prefix+"/tree";
    }



}
