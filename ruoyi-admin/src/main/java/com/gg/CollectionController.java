package com.gg;

import com.gg.domain.Book;
import com.gg.domain.BookCollection;
import com.gg.request.AddBookToCollectionRequest;
import com.gg.service.IBookCollectionService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/book/collection")
public class CollectionController extends BaseController {
    @Autowired
            private IBookCollectionService service;
    String prefix = "book/collection";
    String bookPrefix = "book/collection/book";
    @GetMapping()
    public String collection() {
        return prefix+"/collection";
    }
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list() {
        return getDataTable(service.getAllBookCollection());
    }

    @GetMapping("/add")
    public String add() {
        return prefix+"/add";
    }
   @PostMapping("/add")
   @ResponseBody
   public AjaxResult saveAdd(BookCollection collection) {
        return toAjax(service.createCollection(collection));
   }


   @GetMapping("/book")
    public String collection_book(@RequestParam int collection_id, Model model) {
        model.addAttribute("collection_id",collection_id);
        return prefix+"/collection_book";
   }
   @PostMapping("/book/list")
    @ResponseBody
    public TableDataInfo collection_book_list(@RequestParam int collection_id) {
        return getDataTable(service.getBookInCollection(collection_id));
   }
   @PostMapping("/book/remove")
   @ResponseBody
   public AjaxResult remove_book_from_collection(@RequestParam String compoundId) {
        String[] id =  compoundId.split("_");
        return toAjax(service.removeBookFromCollection(Integer.parseInt(id[0]),Integer.parseInt(id[1])));
   }
   @PostMapping("/book")
   @ResponseBody
    public AjaxResult  addBookToCollection(AddBookToCollectionRequest request) {
        return toAjax(service.addBookToCollection(request.getCollection_id(), request.getIds()));
   }
}
