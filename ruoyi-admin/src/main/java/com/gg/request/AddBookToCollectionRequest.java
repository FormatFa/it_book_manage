package com.gg.request;

import java.util.Arrays;

public class AddBookToCollectionRequest {

    private int collection_id;

    public int getCollection_id() {
        return collection_id;
    }

    public void setCollection_id(int collection_id) {
        this.collection_id = collection_id;
    }

    private String ids;

    public  int[] getIds() {
        String[] idString = ids.split(",");
        int[] result  = new int[idString.length];
        for(int i=0;i<result.length;i++){
            result[i] = Integer.parseInt(idString[i]);
        }
        return result;
    }

    public void setIds(String ids) {
        this.ids = ids;
    }
}
