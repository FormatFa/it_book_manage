package com.gg;

import com.gg.domain.Book;
import com.gg.service.IBookService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("book/manage")
public class BookController extends BaseController {
    @Autowired
  private IBookService bookService;

    String prefix = "book/manage";


    @GetMapping
    public String manage(Model model) {
//        model.addAttribute("books",bookService.getAllBook());
        return prefix+"/manage";
    }
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(Book book) {
        startPage();
        return  getDataTable(bookService.getAllBook(book));
    }
    @GetMapping("/add")
    public String add() {
        return prefix+"/add";
    }
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(@Validated  Book book) {

        logger.info("add book:{}",book);
        return toAjax(bookService.addBook(book));
    }
    @PostMapping("/remove/{book_id}")
    @ResponseBody
    public AjaxResult remove(@PathVariable  int book_id) {
        return toAjax(bookService.deleteBook(book_id));
    }
    @GetMapping("/update/{id}") public String update(@PathVariable int id,Model model) {
        model.addAttribute("book",bookService.getBook(id));
        return prefix+"/edit";}

    @PostMapping("/update")
    @ResponseBody
    public AjaxResult updateSave(@Validated Book book) {
        return toAjax(bookService.updateBook(book));
    }




}
