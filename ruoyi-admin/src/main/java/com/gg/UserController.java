package com.gg;

import com.gg.domain.User;
import com.gg.service.IUserService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/book/user")
public class UserController extends BaseController {
//    TODO update and remove
    String prefix = "book/user";
    @Autowired
    IUserService userService;
    @GetMapping()
    public String user() {
        return prefix+"/user";
    }
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list() {
        startPage();
        return getDataTable(userService.getAllUser());
    }
    @GetMapping("/add")
    public String add() {
        return prefix+"/add";
    }
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult saveAdd(User user) {
        return toAjax(userService.createUser(user));
    }


}
