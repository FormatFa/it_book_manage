#
FROM java:8-jre
ARG JAR_FILE=ruoyi-admin/target/*.jar
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","/app.jar","--spring.profiles.active=prod","--server.port=8020"]