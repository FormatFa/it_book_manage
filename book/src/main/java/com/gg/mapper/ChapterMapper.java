package com.gg.mapper;

import com.gg.domain.Chapter;
import com.gg.domain.Content;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
@Mapper
public interface ChapterMapper {
    List<Chapter> selectChapterByBookId(int book_id);
    Chapter selectChapter(@Param("book_id") int book_id, @Param("chapter_id") int chapter_id);
    int updateChapter(Chapter chapter);
    int saveChapter(Chapter chapter);

    Content selectContent(@Param("book_id") int book_id,@Param("chapter_id") int chapter_id);
    int createOrUpdateContent(Content content);

}
