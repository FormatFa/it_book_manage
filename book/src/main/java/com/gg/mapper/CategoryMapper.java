package com.gg.mapper;

import com.gg.domain.BookCategory;

import java.util.List;

public interface CategoryMapper {
    List<BookCategory> selectAllCategory();
}
