package com.gg.mapper;

import com.gg.domain.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
@Mapper
public interface UserMapper {
    int createUser(User user);
    int deleteUser(int user_id);
    int updateUser(User user);
    List<User> selectAllUser();

}
