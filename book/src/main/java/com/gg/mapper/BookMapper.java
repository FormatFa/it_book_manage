package com.gg.mapper;


import com.gg.domain.Book;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
@Mapper
public interface BookMapper {
    List<Book> selectAllBook(Book searchCondition);
    int createBook(Book book);
    int deleteBook(int id);
    int updateBook(Book book);
    Book selectBookById(int id);



}
