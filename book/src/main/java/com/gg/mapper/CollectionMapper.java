package com.gg.mapper;

import com.gg.domain.Book;
import com.gg.domain.BookCollection;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Collection;
import java.util.List;

@Mapper
public interface CollectionMapper {
    List<BookCollection> selectAllBookCollection();
    int addCollection(BookCollection bookCollection);
    List<Book> selectBookInCollection(int collection_id);

    int addBookToCollection(@Param("collection_id")int collection_id,
                            @Param("book_ids") int[] book_ids);
    int removeBookFromCollection(@Param("collection_id") int collection_id,@Param("book_id") int book_id);
}
