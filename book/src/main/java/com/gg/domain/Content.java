package com.gg.domain;

public class Content {
    private int book_id;
    private int chapter_id;
    private String content;

    public Content(int book_id, int chapter_id, String content) {
        this.book_id = book_id;
        this.chapter_id = chapter_id;
        this.content = content;
    }

    public int getBook_id() {
        return book_id;
    }

    public void setBook_id(int book_id) {
        this.book_id = book_id;
    }

    public int getChapter_id() {
        return chapter_id;
    }

    public void setChapter_id(int chapter_id) {
        this.chapter_id = chapter_id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
