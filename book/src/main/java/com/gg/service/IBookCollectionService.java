package com.gg.service;

import com.gg.domain.Book;
import com.gg.domain.BookCollection;

import java.util.List;

public interface IBookCollectionService {
    List<BookCollection> getAllBookCollection();
    int createCollection(BookCollection collection);
    List<Book> getBookInCollection(int collection_id);

    int addBookToCollection(int collection_id,int[] book_ids);
    int removeBookFromCollection(int collection_id,int book_id);
}
