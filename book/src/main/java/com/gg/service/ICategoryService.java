package com.gg.service;

import com.ruoyi.common.core.domain.Ztree;

import java.util.List;

public interface ICategoryService {
    List<Ztree> getCategoryTree();
}
