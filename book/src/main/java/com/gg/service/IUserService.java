package com.gg.service;

import com.gg.domain.User;

import java.util.List;

public interface IUserService {
    List<User> getAllUser();
    int createUser(User user);
}
