package com.gg.service.impl;

import com.gg.domain.Book;
import com.gg.domain.Chapter;
import com.gg.domain.Content;
import com.gg.mapper.BookMapper;
import com.gg.mapper.ChapterMapper;
import com.gg.service.IBookService;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service

public class BookServiceImpl implements IBookService {
    @Autowired
    private BookMapper bookMapper;
    @Autowired
    private ChapterMapper chapterMapper;

    @Override
    public List<Book> getAllBook(Book searchCondition) {
        return bookMapper.selectAllBook(searchCondition);

    }

    @Override
    public int addBook(Book book) {
        return bookMapper.createBook(book);
    }

    @Override
    public int updateBook(Book book) {
        return bookMapper.updateBook(book);
    }

    @Override
    public int deleteBook(int id) {
        return bookMapper.deleteBook(id);
    }

    @Override
    public Book getBook(int id) {
        return bookMapper.selectBookById(id);
    }

    @Override
    public List<Chapter> getAllChapterByBookId(int book_id) {
        return chapterMapper.selectChapterByBookId(book_id);
    }

    @Override
    public Chapter getChapter(int book_id, int chapter_id) {
        return chapterMapper.selectChapter(book_id,chapter_id);
    }

    @Override
    public int updateChapter(Chapter chapter) {
        return chapterMapper.updateChapter(chapter);
    }

    @Override
    public int createChapter(Chapter chapter) {
        return chapterMapper.saveChapter(chapter);
    }

    @Override
    public int createOrUpdateContent(Content content) {
        return chapterMapper.createOrUpdateContent(content);
    }

    @Override
    public Content getContent(int book_id, int chapter_id) {
        return chapterMapper.selectContent(book_id,chapter_id);
    }
}
