package com.gg.service.impl;

import com.gg.domain.User;
import com.gg.mapper.UserMapper;
import com.gg.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class UserServiceImpl implements IUserService {
    @Autowired
    private UserMapper userMapper;
    @Override
    public List<User> getAllUser() {
        return userMapper.selectAllUser();
    }

    @Override
    public int createUser(User user) {
        return userMapper.createUser(user);
    }
}
