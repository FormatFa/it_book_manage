package com.gg.service.impl;

import com.gg.domain.BookCategory;
import com.gg.mapper.CategoryMapper;
import com.gg.service.ICategoryService;
import com.ruoyi.common.core.domain.Ztree;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CategoryServiceImpl implements ICategoryService {
    @Autowired
    private CategoryMapper categoryMapper;
    @Override
    public List<Ztree> getCategoryTree() {
        List<BookCategory> categories = categoryMapper.selectAllCategory();
        return categories.stream().map(bookCategory -> {
            Ztree ztree = new Ztree();
            ztree.setId((long) bookCategory.getId());
            ztree.setpId((long) bookCategory.getParent_id());
            ztree.setName(bookCategory.getCategory());
            return ztree;
        }).collect(Collectors.toList());
    }
}
