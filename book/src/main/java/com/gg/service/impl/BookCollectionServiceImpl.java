package com.gg.service.impl;

import com.gg.domain.Book;
import com.gg.domain.BookCollection;
import com.gg.mapper.CollectionMapper;
import com.gg.service.IBookCollectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class BookCollectionServiceImpl implements IBookCollectionService {
    @Autowired
    private CollectionMapper collectionMapper;
    @Override
    public List<BookCollection> getAllBookCollection() {
        return collectionMapper.selectAllBookCollection();
    }

    @Override
    public int createCollection(BookCollection collection) {
        return collectionMapper.addCollection(collection);
    }

    @Override
    public List<Book> getBookInCollection(int collection_id) {
        return collectionMapper.selectBookInCollection(collection_id);
    }

    @Override
    public int addBookToCollection(int collection_id, int[] book_ids) {
        return collectionMapper.addBookToCollection(collection_id,book_ids);
    }

    @Override
    public int removeBookFromCollection(int collection_id, int book_id) {
        return collectionMapper.removeBookFromCollection(collection_id,book_id);
    }
}
