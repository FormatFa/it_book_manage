package com.gg.service;

import com.gg.domain.Book;
import com.gg.domain.Chapter;
import com.gg.domain.Content;

import java.util.List;

public interface IBookService {
    List<Book> getAllBook(Book searchCondition);
    int addBook(Book book);
    int updateBook(Book book);
    int deleteBook(int id);
    Book getBook(int id);

    List<Chapter> getAllChapterByBookId(int book_id);
    Chapter getChapter(int book_id,int chapter_id);
    int updateChapter(Chapter chapter);
    int createChapter(Chapter chapter);

    int createOrUpdateContent(Content content);
    Content getContent(int book_id,int chapter_id);
}
