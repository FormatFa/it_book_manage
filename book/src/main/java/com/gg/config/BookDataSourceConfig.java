package com.gg.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

@Configuration
public class BookDataSourceConfig {
    @Autowired
    Environment env;

    @Bean("bookDataSource")
    public DataSource bookDataSource() {
        DriverManagerDataSource dataSource
                = new DriverManagerDataSource();
        dataSource.setDriverClassName(
                env.getProperty("book.datasource.driver-class-name"));
        dataSource.setUrl(env.getProperty("book.datasource.url"));
        dataSource.setUsername(env.getProperty("book.datasource.username"));
        dataSource.setPassword(env.getProperty("book.datasource.password"));

        return dataSource;
    }
}
